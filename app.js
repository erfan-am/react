class Animal{
    constructor(group,name,sound){
        this.arr=[]
        this.name=name
        this.sound=sound
        this.group=group
    };
    addAnimal(group,name,sound){
        const data=this.arr.map(art=>art.reduce((arr,acc)=>acc,[]))
      if(data.find(name=>name.group === group)){
        const FindGroup=data.find(name=>name.group === group );
        FindGroup.members.push({name:name,sound:sound});
        }else{
            this.arr.push([...this.arr,{
                group:group,
                members:[
                    {name:name,sound:sound}
                  ]
            }])
        }
    }
}
const animal=new Animal();

animal.addAnimal('cat','ascar','meow');
animal.addAnimal('cat','alex','meow');
animal.addAnimal('dog','jack','bark');
animal.addAnimal('dog','wood','bark');
animal.addAnimal('dog','moo','bark');
animal.addAnimal('dog','zarg','bark');
console.log(animal.arr.map(art=>art.reduce((arr,acc)=>acc,[])));